export default {
    cocktailOfTheWeek: 'Cocktail of the week',
    cocktails: 'Cocktails',
    steps: 'Preparation',
    details: 'Details',
    time: 'Time',
    ingredients: 'Ingredients',
    basics: 'Basics',
    step: 'Step',
    showAll: 'Show all',
    with: 'with',
    collection: 'Your Cocktails',
    app: 'Our App'
}