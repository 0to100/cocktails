export default {
    cocktailOfTheWeek: 'Cocktail der Woche',
    cocktails: 'Cocktails',
    steps: 'Zubereitung',
    details: 'Details',
    time: 'Zeit',
    ingredients: 'Zutaten',
    basics: 'Grundlagen',
    step: 'Schritt',
    showAll: 'Alle anzeigen',
    with: 'mit',
    collection: 'Deine Cocktails',
    app: 'Unsere App'
}