import createPersistedState from 'vuex-persistedstate'

export default ({store}) => {
  if (!process.env.MOBILE) createPersistedState()(store)
}