import {
    Plugins,
    HapticsImpactStyle
  } from '@capacitor/core';
  
  const { Haptics } = Plugins;

  export default function ({app}, inject) {
      inject('Haptics', Haptics)
  }