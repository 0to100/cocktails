import { Plugins } from '@capacitor/core';

const { Storage } = Plugins;

export default function ({app}, inject) {
    inject('Storage', Storage)
}