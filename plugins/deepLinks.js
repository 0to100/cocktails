import { Plugins } from '@capacitor/core';
const { App } = Plugins;
 
 export default function ({ app }, inject){
    App.addListener('appUrlOpen', function( data ){
        const slug = data.url.split('https://cocktails.ink').pop();
        if( slug ){
        }
        app.router.push({
        path: slug
        });
    })
 }