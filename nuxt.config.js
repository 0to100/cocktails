require('dotenv').config({
	path: process.env.PLATFORM === 'mobile' ? '.env.mobile' : '.env'
})

export default {
	// Target (https://go.nuxtjs.dev/config-target)
	target: 'static',

	// Global page headers (https://go.nuxtjs.dev/config-head)
	head: {
		title: 'Minimal Cocktails',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'initial-scale=1, user-scalable=no, width=device-width, height=device-height, viewport-fit=cover' },
			{ hid: 'description', name: 'description', content: 'Weekly new Cocktails. Minimal Cocktails is a simple cocktail recipe app. Download now on the AppStore and PlayStore' }
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
		],
		script: [
			{
				async: true,
				defer: true,
				'data-website-id': 'ccd3383d-30b4-419b-8c6d-fea35b1e87a6',
				src: 'https://umami.wolves.ink/umami.js',
			},
		]
	},

	// Global CSS (https://go.nuxtjs.dev/config-css)
	css: [
		'~/assets/styles/main.scss'
	],

	// Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
	plugins: [
		{
			src: '~/plugins/haptics.js',
			mode: 'client',
		},
		{
			src: '~/plugins/deepLinks.js',
			mode: 'client',
		},
		{
			src: '~/plugins/storage.js',
			mode: 'client',
		},
		{ 
			src: '~/plugins/persistedState.js',
		 	mode: 'client'
		}
	],

	// Auto import components (https://go.nuxtjs.dev/config-components)
	components: true,

	// Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
	buildModules: [
		'@nuxtjs/color-mode',
		'@/modules/sitemapRouteGenerator'
	],

	// Modules (https://go.nuxtjs.dev/config-modules)
	modules: [
		'nuxt-i18n',
		// https://go.nuxtjs.dev/axios
		'@nuxt/http',
		'@nuxtjs/robots',
		'@nuxtjs/sitemap',
	],

	i18n: {
		locales: [
			{
				code: 'en',
				file: 'en-US.js'
			},
			{
				code: 'de',
				file: 'de-DE.js'
			}
		],
		detectBrowserLanguage: {
			useCookie: false,
			onlyOnRoot: true,
		},
		lazy: true,
		langDir: 'lang/',
		defaultLocale: 'en',
	},

	sitemap: {
		hostname: 'https://cocktails.ink',
		gzip: process.env.PLATFORM === 'mobile' ? false : true,
	},

	robots: {
		Sitemap: 'https://cocktails.ink/sitemap.xml',
		UserAgent: '*',
		Allow: '*',
		Disallow: ['/datenschutz', '/impressum', '/de/impressum', '/de/datenschutz']
	},

	http: {
		baseURL: process.env.API_URL,
		headers: {
			Authorization: 'Bearer ' + process.env.TOKEN
		}
	},

	env: {
		mobile: process.env.PLATFORM === 'mobile' ? true : false
	},

	// Build Configuration (https://go.nuxtjs.dev/config-build)
	build: {
		publicPath: '/nuxt/',
	},
}
