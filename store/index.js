export const state = () => ({
    cocktails: [],
    alcohol: [],
    collection: [],
    cocktailOfTheWeek: null
})

export const actions = {
    async nuxtServerInit({ dispatch }) {
        await dispatch('getCocktails')
        await dispatch('getAlcohol')
        await dispatch('getCocktailOfTheWeek')
    },
    async getCocktails({ commit, dispatch }) {
        let cocktailData;
        if (this.$i18n.locale === 'de') {
            cocktailData = await this.$http.$post('/api/collections/get/Cocktails', {
                lang: 'de'
            })
        } else {
            cocktailData = await this.$http.$post('/api/collections/get/Cocktails', {
                lang: 'en'
            })
        }
        const cocktails = cocktailData.entries
		cocktails.forEach((cocktail) => {
			commit('addCocktail', cocktail)
        })
        dispatch('save')
    },
     async getCocktailOfTheWeek({ commit, dispatch }) {
        let cotw = await this.$http.$get('/api/singletons/get/CotW')
        commit('setCockctailOfTheWeek', cotw)
        dispatch('save')
    },
    async getAlcohol({ commit, dispatch }) {
        let alcoholData;
        if (this.$i18n.locale === 'de') {
            alcoholData = await this.$http.$post('/api/collections/get/Alcohol', {
                lang: 'de'
            })
        } else {
            alcoholData = await this.$http.$post('/api/collections/get/Alcohol', {
                lang: 'en'
            })
        }
        const alcohol = alcoholData.entries
		alcohol.forEach((bottle) => {
			commit('addAlcohol', bottle)
        })
        dispatch('save')
    },
    saveCocktail({ commit, dispatch }, id) {
        commit('saveCocktail', id)
        dispatch('save')
    },
    save({ state }) {
        if (process.env.mobile) {
			this.$Storage.set({
				key: 'store',
				value: JSON.stringify(state),
			})
        }
    }
}

export const mutations = {
    init(state, {key, value}) {
        state[key] = value
    },
    addCocktail(state, cocktail) {
        const i = state.cocktails.findIndex(a => a._id === cocktail._id)
        if (i !== -1) {
            state.cocktails[i] = cocktail
        } else {
            state.cocktails.push(cocktail)
        }
    },
     setCockctailOfTheWeek(state, cocktail) {
        state.cocktailOfTheWeek = cocktail
    },
    addAlcohol(state, alcohol) {
        const i = state.alcohol.findIndex(a => a._id === alcohol._id)
        if (i !== -1) {
            state.alcohol[i] = alcohol
        } else {
            state.alcohol.push(alcohol)
        }
    },
    setCollection(state, collection) {
        if (Array.isArray(collection)) {
            state.collection = collection
        }
    },
    saveCocktail(state, id) {
        const i = state.collection.indexOf(id)
        if (i !== -1) {
            state.collection.splice(i, 1);
        } else {
            state.collection.push(id)
        }
    }
}

export const getters = {
    getCocktail(state) {
        return (slug) => {
            return state.cocktails.find(c => c.name_slug === slug)   
        }
    },
    getCocktailById(state) {
        return (id) => {
            return state.cocktails.find(c => c._id === id)   
        }
    },
    getBottle(state) {
        return (id) => {
            return state.alcohol.find(a => a._id === id)
        }
    },
    getCocktails(state) {
        return [...state.cocktails]
            .sort((a, b) => b._created - a._created)
    },
    getCocktailOfTheWeek(state) {
        return state.cocktails.find(c => c._id === state.cocktailOfTheWeek?.cocktail?._id)   
        
    },
}

